const minus = require('./minus.js');

test('2-1 equal 1', () => {
    const value = minus(2,1);
    expect(value).toBe(1);
});

const faker = require('faker');

function getFakeCard(id) {
    faker.seed(id);
    const v = faker.helpers.createCard();
    return v;
}

module.exports = getFakeCard;